const routes = [
    // {
    //     path: "/",
    //     name: "index",
    //     component: require("./components/App.vue").default
    // },
    {
        path: "/football",
        name: "index",
        component: require("./components/App.vue").default
    },
    {
        path: "/football/liga/:id?",
        name: "gotoLiga",
        component: require("./components/Liga/").default
    },
    {
        path: "/football/team/:id?",
        name: "gotoTeam",
        component: require("./components/Team/").default
    },
    //
    {
        path: "/football/match",
        name: "gotoMatch",
        component: require("./components/Match/").default
    },
    {
        path: "/football/match/edit/:id?",
        name: "gotoMatchEdit",
        component: require("./components/Match/Edit").default
    },
    {
        path: "/users/:id?",
        name: "gotoUser",
        component: require("./components/User/").default
    },
    {
        path: "/domain/:id?",
        name: "gotoDomain",
        component: require("./components/ClientDomain/").default
    }
];

export default routes;
