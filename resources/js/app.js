/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");

import VueRouter from "vue-router";
import VueToastr from "vue-toastr";
import VueConfirmDialog from 'vue-confirm-dialog'

import UUID from "vue-uuid";
// import VuePaginate from 'vue-paginate'
let Paginate = require("vuejs-paginate");
// Vue.use(require("vue-moment"));

import Multiselect from "vue-multiselect";

import "vue-multiselect/dist/vue-multiselect.min.css";
Vue.component("multiselect", Multiselect);
Vue.component("paginate", Paginate);

// Vue.component('paginate', Paginate)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component(
    "example-component",
    require("./components/ExampleComponent.vue").default
);
Vue.component("navbar", require("./components/Navbar.vue").default);

// Vue.component('paginate', require(Paginate));

Vue.config.productionTip = false;

Vue.use(VueRouter);
Vue.use(UUID);
Vue.use(VueConfirmDialog);
Vue.component('vue-confirm-dialog', VueConfirmDialog.default)
// Vue.use(VuePaginate)
Vue.use(VueToastr, {
    defaultTimeout: 3000,
    defaultProgressBar: false,
    defaultProgressBarValue: 0,
    defaultType: "success",
    defaultPosition: "toast-top-right",
    defaultCloseOnHover: false,
    defaultStyle: { "background-color": "#282c34" },
    defaultClassNames: ["animated", "zoomInUp"]
});

import routes from "./routes";

const router = new VueRouter({
    mode: "history",
    routes
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: "#app",
    router,
    components: {
        navbar: require("./components/Navbar.vue").default
    }
});
