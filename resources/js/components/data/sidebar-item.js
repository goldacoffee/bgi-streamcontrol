const data = [
    {
        title: "Football",
        data: [
            {
                name: "Liga",
                route: "gotoLiga",
                icon: "fas fa-user-cog"
            },
            {
                name: "Team",
                route: "gotoTeam",
                icon: "fas fa-users"
            },
            {
                name: "Match",
                route: "gotoMatch",
                icon: "fas fa-sitemap"
            }
        ]
    },
    {
        title: "General Settings",
        data: [
            // {
            //     name: "Users",
            //     route: "gotoUser",
            //     icon: "fas fa-user"
            // },
            {
                name: "Domain",
                route: "gotoDomain",
                icon: "fas fa-people-arrows"
            }
        ]
    }
];

export default data;
