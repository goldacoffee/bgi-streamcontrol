@extends('layouts.app')

@section('title', 'Stream Control')

@section('content')

<section class="section">

  <div class="section-body">
    <router-view></router-view>
  </div>
</section>
@endsection


