<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('werapp_football_matches', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('liga_id');
            $table->string('name');
            $table->string('featured_image')->nullable();
            $table->uuid('home_team_id');
            $table->uuid('away_team_id');
            $table->string('slug')->nullable();
            $table->string('status');
            $table->text('stream_link');
            $table->dateTime('start_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('werapp_football_matches');
    }
}
