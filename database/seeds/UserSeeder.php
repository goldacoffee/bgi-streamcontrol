<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $x = new User(['id' => Str::uuid()]);
        $x->name = 'BGI';
        $x->email = 'admin@gmail.com';
        $x->level = 'admin';
        $x->api_token = Str::random(60);
        $x->password = Hash::make('password');
        $x->save();
    }
}
