<?php

use App\WerApp\Football\FootballLiga;
use App\WerApp\Football\FootballTeam;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = $this->seedData();
        foreach ($data as $data) {
            $fill = [
                'liga_id' => $this->getLigaId($data[1]),
                'name' => $data[0],
                'logo' => $data[2],
            ];


            $x =  new FootballTeam(['id' => Str::uuid()]);
            $x->fill($fill);
            $x->save();

            print_r($fill['name'] . "-" . $fill['liga_id'] . " > added to team  \n");
        }
    }

    private function getLigaId($ligaName)
    {
        $liga = FootballLiga::where('name', $ligaName)->first();
        return $liga->id;
    }

    private function seedData()
    {
        return ([
            ["Arsenal", "Liga Inggris", "https://1.bp.blogspot.com/-N_8UGmO_IXw/XtWO0kGyCtI/AAAAAAAAKvg/soCic8rgB8w87ufQGWI0eKBr8mMvyE2sACLcBGAsYHQ/s1600/arsenal.png"],
            ["Aston Villa", "Liga Inggris", "https://1.bp.blogspot.com/-Iu2ygFhGZII/XtWO0tuaj4I/AAAAAAAAKvc/i5vNE0z4sZsChmlJ1Wgsk-6n4TkZbkbCQCLcBGAsYHQ/s1600/aston.png"],
            ["Bournemouth", "Liga Inggris", "https://1.bp.blogspot.com/-xpeMTysqoNg/XtWO1dPUvgI/AAAAAAAAKvk/YoO9IZ1vzKE67L4wOGGEX-XbwaQb4ZYLgCLcBGAsYHQ/s1600/bournemouth.png"],
            ["Brighton Albion", "Liga Inggris", "https://1.bp.blogspot.com/-gVakJwmLgWY/XtWO11nEZYI/AAAAAAAAKvo/9r7IydQ8QmQP_B1fR3k43pYXQI7n_B-qgCLcBGAsYHQ/s1600/brighton.png"],
            ["Burnley", "Liga Inggris", "https://1.bp.blogspot.com/-JJfnnyHNvRE/XtWO11z_XBI/AAAAAAAAKvs/GJuBCp-e5yk-wzrHdip2AtbM9VCxS-JXACLcBGAsYHQ/s1600/burnley.png"],
            ["Chelsea", "Liga Inggris", "https://1.bp.blogspot.com/-1RNexihMOro/XtWO2DGFueI/AAAAAAAAKvw/KpKWra2nqZMq0GEVWCbz6VciyB7gnEmxQCLcBGAsYHQ/s1600/chelsea.png"],
            ["Crystal Palace", "Liga Inggris", "https://1.bp.blogspot.com/-_qqg6nKxcic/XtWO2-GmrfI/AAAAAAAAKv4/uePPoTeR_oMNIYegMv4X2_gw9mMWmJADACLcBGAsYHQ/s1600/crystal.png"],
            ["Everton", "Liga Inggris", "https://1.bp.blogspot.com/-lgZ1uKXf9V0/XtWO26K9ghI/AAAAAAAAKv0/HJbRRzMkJagaMtNJCGYnJV_g0_1IubfdgCLcBGAsYHQ/s1600/everton.png"],
            ["Leicester", "Liga Inggris", "https://1.bp.blogspot.com/-cEnmXatdLvk/XtWO3GPIYkI/AAAAAAAAKv8/4viKlV-tse46vi0poBF_r3RTs4vDBR1gwCLcBGAsYHQ/s1600/leicester.png"],
            ["Liverpool", "Liga Inggris", "https://1.bp.blogspot.com/-LT2yuAjzJww/XtWO3r-QbrI/AAAAAAAAKwA/IVOwXNHR34gSn43njgnSUh3iKzFjQd-gACLcBGAsYHQ/s1600/liverpool.png"],
            ["Manchester City", "Liga Inggris", "https://1.bp.blogspot.com/-Tsmqcz13BAM/XtWO31GFoFI/AAAAAAAAKwE/oLA97Gx7MWg1XFOSjWJey6P9Lp7UbD_wACLcBGAsYHQ/s1600/mc.png"],
            ["Manchester United", "Liga Inggris", "https://1.bp.blogspot.com/-jopc1-gsJ-w/XtWO4I2mdOI/AAAAAAAAKwI/EAqXpG3DiqEVIGzkFkMoe2-tvZidICjfQCLcBGAsYHQ/s1600/mu.png"],
            ["Newcaste United", "Liga Inggris", "https://1.bp.blogspot.com/-9C2YGozKg7M/XtWO4l_svEI/AAAAAAAAKwM/ir4hPgmv6FoXmyrL1iANAu9pfx5qMgS4gCLcBGAsYHQ/s1600/newcastle.png"],
            ["Norwich City", "Liga Inggris", "https://1.bp.blogspot.com/-f1t9cZ4ls3Y/XtWO46OyQvI/AAAAAAAAKwQ/Xfz7C7SoZvkNKdu7hz79utr-5iatjqJEACLcBGAsYHQ/s1600/norwich.png"],
            ["Sheffield United", "Liga Inggris", "https://1.bp.blogspot.com/-2eRWE3xCDhw/XtWO5IAyK7I/AAAAAAAAKwU/yL_sGHJbbbcDXsehfOlwwvXz0c-DjXRkQCLcBGAsYHQ/s1600/sheffieldunited.png"],
            ["Southampton", "Liga Inggris", "https://1.bp.blogspot.com/-CXJMtI48cXQ/XtWO5v_nsnI/AAAAAAAAKwY/Y1NsgG2siK4J-xm3gPwvQKk7AJR8Z3NxwCLcBGAsYHQ/s1600/southampton.png"],
            ["Tottenham Hotspur", "Liga Inggris", "https://1.bp.blogspot.com/-TczlsPOJDyA/XtWO59jWadI/AAAAAAAAKwc/WZpNlZVPdR41FZtDI1cjgWUlB_E2cfmvwCLcBGAsYHQ/s1600/tth.png"],
            ["Watford", "Liga Inggris", "https://1.bp.blogspot.com/-RAnr0JqJLwA/XtWO576_oBI/AAAAAAAAKwg/0j4FeL41fFYqLQnbhGxZ8E0_DZLUkfMrQCLcBGAsYHQ/s1600/watford.png"],
            ["West ham United", "Liga Inggris", "https://1.bp.blogspot.com/-kdpBNa2TFr4/XtWO6Y_eN1I/AAAAAAAAKwk/Qpk3AvYkB0kEB67TynxRf7NSnbPtB3JXwCLcBGAsYHQ/s1600/westham.png"],
            ["Wolves", "Liga Inggris", "https://1.bp.blogspot.com/-KtE6bkJCccQ/XtWO6vfL6AI/AAAAAAAAKwo/d17wcH5Cg3oQGkIIzwyjZ6i6BISYcQtSwCLcBGAsYHQ/s1600/wolves.png"],
            ["AC Milan", "Liga italia", "https://1.bp.blogspot.com/-jWH5nvg_EPs/XtWPG1TNLWI/AAAAAAAAKww/b_rJjaaaFEUQzCrlWcW4XqUfOu-BKGqsACLcBGAsYHQ/s1600/acmilan.png"],
            ["Atalanta", "Liga italia", "https://1.bp.blogspot.com/-qlJy4syIARI/XtWPGyUMezI/AAAAAAAAKw0/U-pdZ8qYOlMfJ1aymfFEhmcBT1MPS1i2ACLcBGAsYHQ/s1600/atalanta.png"],
            ["Bologna", "Liga italia", "https://1.bp.blogspot.com/-rz1aZg1gsJ0/XtWPHIORjrI/AAAAAAAAKw4/C76vrGhTdOY-au8stF9L2LqB4kU2REnBgCLcBGAsYHQ/s1600/bologna.png"],
            ["Brescia", "Liga italia", "https://1.bp.blogspot.com/-yfkK_I0CO0o/XtWPH8tIX2I/AAAAAAAAKw8/90_wMBiGseMKdwGJq6ZtSpEAPW9k1_W4wCLcBGAsYHQ/s1600/brescia.png"],
            ["Cagliari", "Liga italia", "https://1.bp.blogspot.com/-UKiXp9Goj8M/XtWPIUvcP5I/AAAAAAAAKxA/aZdcpHLnqWIp5ZElKTIYmHDdEyNdgC8uwCLcBGAsYHQ/s1600/cagliari.png"],
            ["Fiorentina", "Liga italia", "https://1.bp.blogspot.com/-3VgpuI61Ets/XtWPIhI9CeI/AAAAAAAAKxI/OePwIreaETgoHSFq-CV5D7LzAnAzdxMRQCLcBGAsYHQ/s1600/fiorentina.png"],
            ["Genoa", "Liga italia", "https://1.bp.blogspot.com/-bGZZ6MnXbM4/XtWPI3LqbYI/AAAAAAAAKxM/W07WH66LQZwNV_So142oEBLT5BTjfzSoACLcBGAsYHQ/s1600/genoa.png"],
            ["Inter Milan", "Liga italia", "https://1.bp.blogspot.com/-LZ2hqCr0uLo/XtWPJZHLQ4I/AAAAAAAAKxQ/V-fFCxfa54sLoU_-oGC5YqN3jiAA1qibgCLcBGAsYHQ/s1600/inter.png"],
            ["Juventus", "Liga italia", "https://1.bp.blogspot.com/-cH8KGglr1I4/XtWPKOwJ3JI/AAAAAAAAKxU/pTCL8fgpM6cNCwCK4DydLN6dQCcOkj7UgCLcBGAsYHQ/s1600/juventus.png"],
            ["Lazio", "Liga italia", "https://1.bp.blogspot.com/-l3rUBZJBV3M/XtWPKlxn8tI/AAAAAAAAKxY/wtCWsA_Whlwf8EsRD248sTLVjoVwDT0QACLcBGAsYHQ/s1600/lazio.png"],
            ["Lecce", "Liga italia", "https://1.bp.blogspot.com/-VriFRpsq2os/XtWPLIiwOiI/AAAAAAAAKxg/SqvQzaQ5RVEqY0q0nldVWnSoLHkSRonYACLcBGAsYHQ/s1600/lecce.png"],
            ["Napoli", "Liga italia", "https://1.bp.blogspot.com/-4upZViLGUig/XtWPL_y_uMI/AAAAAAAAKxk/D6AJw1-QhTYEF3tEtSSnRVBpI-aiE8brQCLcBGAsYHQ/s1600/napoli.png"],
            ["Parma", "Liga italia", "https://1.bp.blogspot.com/-XMP2PrAuIXU/XtWPL7hbHdI/AAAAAAAAKxo/OZIOjb23O-kzr9Ipm7s3m5nPr7RsM4MfwCLcBGAsYHQ/s1600/parma.png"],
            ["AS Roma", "Liga italia", "https://1.bp.blogspot.com/-DccR1wtFoeU/XtWPNc8AqMI/AAAAAAAAKxw/uaY8rRx68UwudT60JTxwyAHUT2WJx8WiACLcBGAsYHQ/s1600/roma.png"],
            ["Sampdoria", "Liga italia", "https://1.bp.blogspot.com/-gP1awkwuohw/XtWPN8T0dFI/AAAAAAAAKx0/CheoXRO5a7QK-eCErDOu8V1kTggyAW2agCLcBGAsYHQ/s1600/sampdoria.png"],
            ["Sassuolo", "Liga italia", "https://1.bp.blogspot.com/-QnC7jT9LS04/XtWPNyZL0GI/AAAAAAAAKx4/skxtjEVcVyEjHu66Mc5OU5s0VIHw1mDqQCLcBGAsYHQ/s1600/sassuolo.png"],
            ["SPAL", "Liga italia", "https://1.bp.blogspot.com/-gcG-eJf4zvg/XtWPOoqEkAI/AAAAAAAAKyA/g8PVSpKQ6nMa2Zy70Nc-_EcZbcClJXyugCLcBGAsYHQ/s1600/spal.png"],
            ["Torino", "Liga italia", "https://1.bp.blogspot.com/-HjToMDXZjxs/XtWPOmZ1DFI/AAAAAAAAKyE/QyUV7InQLyA_-mYPKGfTQKa8IRDr2l2GACLcBGAsYHQ/s1600/torino.png"],
            ["Udinese", "Liga italia", "https://1.bp.blogspot.com/-d0S0Yi9iamE/XtWPPQxSe6I/AAAAAAAAKyI/E54Y4DKKM9oad4LxYSmRlwCJ_je3bPChQCLcBGAsYHQ/s1600/udinese.png"],
            ["Verona", "Liga italia", "https://1.bp.blogspot.com/-yOoc3HCk5Aw/XtWPPpFNMII/AAAAAAAAKyM/G6sEDIVW6mgtJvMiFvElhBtCXx5MsaKVwCLcBGAsYHQ/s1600/verona.png"],
            ["Augsburg", "Liga Jerman", "https://1.bp.blogspot.com/-DmZwFlN1TVo/XtWPY8B-asI/AAAAAAAAKyQ/gTrnOQwkbMg_z7Z0dU9ZI9iRoMnvOGLDACLcBGAsYHQ/s1600/augsburg.png"],
            ["Bayern Munchen", "Liga Jerman", "https://1.bp.blogspot.com/--9onYVH1zF8/XtWPZEGE6gI/AAAAAAAAKyU/QB455JPblYcs6FjPdbKwxvUztYSpbA2kgCLcBGAsYHQ/s1600/bayern.png"],
            ["Werder Bremen", "Liga Jerman", "https://1.bp.blogspot.com/-lzWlgez86kc/XtWPZmv5izI/AAAAAAAAKyc/rkLkU74VpWEtvitk9Cu97Q68GUqKtO4cQCLcBGAsYHQ/s1600/bremen.png"],
            ["Borussia Dortmund", "Liga Jerman", "https://1.bp.blogspot.com/-uX2OHtd31Ds/XtWPaQ2iV_I/AAAAAAAAKyk/iq0HCgczx1oAebQlGApulVsZTy7DgPelQCLcBGAsYHQ/s1600/dortmund.png"],
            ["Fortuna Dusseldorf", "Liga Jerman", "https://1.bp.blogspot.com/-_-mUxinDzVI/XtWPapoq4WI/AAAAAAAAKyo/WmAX3c_VGIEFbXN9Tum4qRCIIaSYRE-2gCLcBGAsYHQ/s1600/fortuna.png"],
            ["Frankfurt", "Liga Jerman", "https://1.bp.blogspot.com/-oKTCzvHIq7Y/XtWPa6G8-lI/AAAAAAAAKys/cS9yw8HTFUYopg5_jwEen2FMYUf9uXLvgCLcBGAsYHQ/s1600/frankfurt.png"],
            ["Freiburg", "Liga Jerman", "https://1.bp.blogspot.com/-56ZM8x0J4i8/XtWPbB0Pz0I/AAAAAAAAKyw/qXyEmWDxxeQflT71we0T049BiM7VzRlBQCLcBGAsYHQ/s1600/freiburg.png"],
            ["B. Moenchengladbach", "Liga Jerman", "https://1.bp.blogspot.com/-sJnTJJRWw-U/XtWPblHcZCI/AAAAAAAAKy0/xnsaNZjusUYWsdWwEmQd6HT2zO78KV4MQCLcBGAsYHQ/s1600/gladbach.png"],
            ["Hertha Berlin", "Liga Jerman", "https://1.bp.blogspot.com/-KySFr3MG-IM/XtWPb_hWtLI/AAAAAAAAKy4/DowYtYTyXcM45lDIhk8pAQ-WqE7M-w1AwCLcBGAsYHQ/s1600/hertha.png"],
            ["TSG Hoffenheim", "Liga Jerman", "https://1.bp.blogspot.com/-yd5Mqa7qvqE/XtWPcJ9n1RI/AAAAAAAAKy8/NmI_P6JBrGIoTXmFy7Bf0geapG5sd9zDACLcBGAsYHQ/s1600/hoffenheim.png"],
            ["FC Koln", "Liga Jerman", "https://1.bp.blogspot.com/-ky90jbOtBQc/XtWPckXvTNI/AAAAAAAAKzA/2EarRQmDaasBoHCM1DVIzq-nsqfj5PvKQCLcBGAsYHQ/s1600/koln.png"],
            ["RB Leipzig", "Liga Jerman", "https://1.bp.blogspot.com/-7Zej5MPammg/XtWPdL80iAI/AAAAAAAAKzI/aHUFA7QlvRwHK_p5kf65nGBR92QwbrbnQCLcBGAsYHQ/s1600/leipzig.png"],
            ["Bayer Leverkusen", "Liga Jerman", "https://1.bp.blogspot.com/-s5or8FL_h5U/XtWPdeYr3hI/AAAAAAAAKzM/s1Y4fbt0wg4M4q8L4ZePPg2oMrvsxzRfACLcBGAsYHQ/s1600/leverkusen.png"],
            ["Mainz", "Liga Jerman", "https://1.bp.blogspot.com/-_ORAnCCIpI4/XtWPZDbfUiI/AAAAAAAAKyY/d6-6RCLDPl8TJ2DTGYe42l5QIRdKogoxwCLcBGAsYHQ/s1600/Mainz.png"],
            ["SC Padeborn 07", "Liga Jerman", "https://1.bp.blogspot.com/-UagwcylDRWc/XtWPdRDhI5I/AAAAAAAAKzQ/lESnCvxN82kGEh2AtFDekiPG4Bx03p4aACLcBGAsYHQ/s1600/padeborn.png"],
            ["Schalke 04", "Liga Jerman", "https://1.bp.blogspot.com/-3NStft2sjK0/XtWPd7kzUDI/AAAAAAAAKzU/8C0JLDbXm5gx4epDdke4zEFPVa1hnKy_wCLcBGAsYHQ/s1600/schalke.png"],
            ["Union Berlin", "Liga Jerman", "https://1.bp.blogspot.com/-EbilLs18meE/XtWPeWBBQNI/AAAAAAAAKzc/Ulvsrl0WCTcfBNxKhzmPGhEI9zy-vbl4ACLcBGAsYHQ/s1600/union.png"],
            ["Wolfsburg", "Liga Jerman", "https://1.bp.blogspot.com/-TF07GnphuuI/XtWPeU2ECtI/AAAAAAAAKzg/U1xljyAoto0IE3Fklxm-4852nL3bzAdOQCLcBGAsYHQ/s1600/wolfsburg.png"],
            ["Alaves", "Liga Spanyol", "https://1.bp.blogspot.com/-TtPDuLofqxM/XtWPogN3DqI/AAAAAAAAKzs/iNfVYKrT3_kuo1_qfr6GrsvaROhz9xvmwCLcBGAsYHQ/s1600/alaves.png"],
            ["Atletico Madrid", "Liga Spanyol", "https://1.bp.blogspot.com/-sp5iaWDYmzs/XtWPomn7taI/AAAAAAAAKzo/lxMb8_auacUpLzNd9h0lQb-c7iODkCwAgCLcBGAsYHQ/s1600/atm.png"],
            ["Barcelona", "Liga Spanyol", "https://1.bp.blogspot.com/-XmtF2w3FnUI/XtWPpGGjKsI/AAAAAAAAKzw/2v1c0qsfmcwpzE6UmQX3IBVPPJuR9bKKQCLcBGAsYHQ/s1600/barca.png"],
            ["Real Betis", "Liga Spanyol", "https://1.bp.blogspot.com/-tta1QwN_M48/XtWPpmn6V1I/AAAAAAAAKz0/0OAU0QwkNyYGuSe3UjD_yBFJB2R_eFdOQCLcBGAsYHQ/s1600/betis.png"],
            ["Athletic Bilbao", "Liga Spanyol", "https://1.bp.blogspot.com/-acY71G3AcDA/XtWPpgQ2VjI/AAAAAAAAKz4/MEOyOQXm3xEfqAHCsgvXOVTJcKFXBDjkgCLcBGAsYHQ/s1600/bilbao.png"],
            ["Celta Vigo", "Liga Spanyol", "https://1.bp.blogspot.com/-lW9zxwgwQCI/XtWPp55RYLI/AAAAAAAAKz8/agnXvvhs4dYyJssB_bs6fH88ynUU9iAYgCLcBGAsYHQ/s1600/celtavigo.png"],
            ["Eibar", "Liga Spanyol", "https://1.bp.blogspot.com/-HE7VbiO0yPE/XtWPqf63a9I/AAAAAAAAK0A/HrgoEC1xdXc6NamKMJBPyUg4WI9xpQFqgCLcBGAsYHQ/s1600/eibar.png"],
            ["Espanyol", "Liga Spanyol", "https://1.bp.blogspot.com/-TDCV5BDnk3c/XtWPqpeeL0I/AAAAAAAAK0E/h8XWHF-eAMoM49Mxqw6WF-hP1lLl9vNaACLcBGAsYHQ/s1600/espanyol.png"],
            ["Getafe", "Liga Spanyol", "https://1.bp.blogspot.com/-yvTDkHynwxo/XtWPrLgbXxI/AAAAAAAAK0I/__RuGhOsxwom24HpI437yY1vG3Ivsyu5QCLcBGAsYHQ/s1600/getafe.png"],
            ["Granada", "Liga Spanyol", "https://1.bp.blogspot.com/-OgpL2BQGgNU/XtWPrcwmnQI/AAAAAAAAK0M/8Qd-N_EHfkYGbfoqp7OwFotN7Pz7wxFXgCLcBGAsYHQ/s1600/granada.png"],
            ["Leganes", "Liga Spanyol", "https://1.bp.blogspot.com/-Yv-5DjJmDro/XtWPrhuvnsI/AAAAAAAAK0Q/OjycbBDw8yAO9e79rLrN13f5qnOfs13JwCLcBGAsYHQ/s1600/leganes.png"],
            ["Levante", "Liga Spanyol", "https://1.bp.blogspot.com/-pH0JjVq3vOI/XtWPr4QonxI/AAAAAAAAK0U/insxxtJ4aP4nN2fgJbeHLO62tX7Vu9qlwCLcBGAsYHQ/s1600/levante.png"],
            ["Mallorca", "Liga Spanyol", "https://1.bp.blogspot.com/-RDdU1u7eiCY/XtWPsHek17I/AAAAAAAAK0Y/Kx0VUdiMr1kyYpIUQdjtzu8jrBI6Luq5wCLcBGAsYHQ/s1600/mallorca.png"],
            ["Osasuna", "Liga Spanyol", "https://1.bp.blogspot.com/-sEQHgRPNrMs/XtWPsZbgWAI/AAAAAAAAK0c/L1uDq6jrT3kMGP8sENT-E3oCcJEJYtPcwCLcBGAsYHQ/s1600/osasuna.png"],
            ["Real Madrid", "Liga Spanyol", "https://1.bp.blogspot.com/-FOgVvvThn6Y/XtWPsiH86mI/AAAAAAAAK0g/rieOt8OQUnYUYxw5YqQLcxrzECxxsnwPgCLcBGAsYHQ/s1600/rm.png"],
            ["Sevilla", "Liga Spanyol", "https://1.bp.blogspot.com/-2syX8q7aAcY/XtWPtJhbq7I/AAAAAAAAK0k/DPb2ybHwOD8K3m1uz_LqSSMKfhrg9Ys5gCLcBGAsYHQ/s1600/sevilla.png"],
            ["Real Sociedad", "Liga Spanyol", "https://1.bp.blogspot.com/-0MC-tX9QksA/XtWPtHFLdGI/AAAAAAAAK0o/lq3ISb-wGCwBw6BjYWGBM-p0O4i7gQzPgCLcBGAsYHQ/s1600/sociedad.png"],
            ["Valencia", "Liga Spanyol", "https://1.bp.blogspot.com/-Yfq2lwrTf9o/XtWPtQsolOI/AAAAAAAAK0s/H57nr04gz9EFQhGsrYeMDzXj15TrRrP0ACLcBGAsYHQ/s1600/valencia.png"],
            ["Valladolid", "Liga Spanyol", "https://1.bp.blogspot.com/-A2EDGXRywjk/XtWPt2Sl85I/AAAAAAAAK0w/SvYcJ33wXtYCWEjd6nIFeUPHXIrzFkD2wCLcBGAsYHQ/s1600/valladolid.png"],
            ["Villarreal", "Liga Spanyol", "https://1.bp.blogspot.com/-XIp3Ku0dxNU/XtWPuKeFLPI/AAAAAAAAK00/RLea4p-LgqkONxN6VJ3bkbAKuCJ-WJy9ACLcBGAsYHQ/s1600/villarreal.png"],
            ["Lyon", "Liga Prancis", "https://1.bp.blogspot.com/-KVQpn604vQ8/XtWP3VtAJnI/AAAAAAAAK1E/f0RRaY_Y7RkBbLWVKqp46lEUeQpHL-huQCLcBGAsYHQ/s1600/lyon.png"],
            ["PSG", "Liga Prancis", "https://1.bp.blogspot.com/-phWMTxw-cTY/XtWP3cYBhGI/AAAAAAAAK1I/Y5gB9CAIPKkQ8sqZkZYcGPPio1IIkYmGwCLcBGAsYHQ/s1600/PSG.png"],
            ["Brest", "Liga Prancis", "https://1.bp.blogspot.com/-HudrtXcbyW8/XteJdi4zwEI/AAAAAAAAK30/kD_SVNyjztcr7dGIMEFtcB6-GpJTyUU9gCLcBGAsYHQ/s1600/brest.png"],
            ["Angers", "Liga Prancis", "https://1.bp.blogspot.com/-b5R6LabMGX4/XteJb-xVASI/AAAAAAAAK3s/t_8OEMjF3ZIeUCFBDidJD0hpVwTVaVwBQCLcBGAsYHQ/s1600/angers.png"],
            ["Bordeaux", "Liga Prancis", "https://1.bp.blogspot.com/-3McDaMTAxiM/XteJcCIi5SI/AAAAAAAAK3w/IigPMeENjGYL3LPB0MbsQRPBCqcV0tV0QCLcBGAsYHQ/s1600/bordeaux.png"],
            ["Dijon", "Liga Prancis", "https://1.bp.blogspot.com/-XSpR-p1gQLA/XteJeLCqx9I/AAAAAAAAK34/eXe46-AsJZMixFIdjCQSx6sa4hIlEJhTQCLcBGAsYHQ/s1600/dijon.png"],
            ["Saint-Etienne", "Liga Prancis", "https://1.bp.blogspot.com/-_WsM1eABJ6g/XteJetCcOOI/AAAAAAAAK38/3GurlJYXZ-sg0-5Z8YOKsuvX42D6ZKlnQCLcBGAsYHQ/s1600/etienne.png"],
            ["AS Monaco", "Liga Prancis", "https://1.bp.blogspot.com/-6dsVaoEm-4w/XteJgN04y_I/AAAAAAAAK4M/cuoZu8rlhpcmpG0RNUQPWtfJaYFgfgTvwCLcBGAsYHQ/s1600/monaco.png"],
            ["Metz", "Liga Prancis", "https://1.bp.blogspot.com/-_hyF2qBNbYE/XteJfkOtuYI/AAAAAAAAK4I/BQ2dkUxyOYMZkhm7O75IGwKRg0hDf3cZgCLcBGAsYHQ/s1600/metz.png"],
            ["Marseille", "Liga Prancis", "https://1.bp.blogspot.com/-b-_r2mC7QK0/XteJZlaIFqI/AAAAAAAAK3g/iB2vE84qOusSRxQvYCKXYazssjs_5U3KwCLcBGAsYHQ/s1600/Marseille.png"],
            ["Lorient", "Liga Prancis", "https://1.bp.blogspot.com/-L_CKlEYxyB0/XteJfNJXocI/AAAAAAAAK4E/MU-jL5aDd3MOl3TapwbooYlkUqnJyXo2ACLcBGAsYHQ/s1600/lorient.png"],
            ["Lille", "Liga Prancis", "https://1.bp.blogspot.com/-wToL5CwLVt8/XteJZHu2UZI/AAAAAAAAK3c/QwLiw8KZoEwLDPjAHrvwTnLkdlXUzx_fQCLcBGAsYHQ/s1600/Lille.png"],
            ["RC Lens", "Liga Prancis", "https://1.bp.blogspot.com/-aXBi9S3TA1E/XteJe5aX9PI/AAAAAAAAK4A/BexnwqaItwod0zBKA_nnCtLmWwPdrw0VACLcBGAsYHQ/s1600/lens.png"],
            ["Montpellier", "Liga Prancis", "https://1.bp.blogspot.com/-lDBMfSL9lmk/XteJgXTP1hI/AAAAAAAAK4Q/CeIMahIym7w3TB9fqWNMs22K4hmkJWggACLcBGAsYHQ/s1600/montpellier.png"],
            ["Nantes", "Liga Prancis", "https://1.bp.blogspot.com/-HWcAztAenRU/XteJgoYoBiI/AAAAAAAAK4U/M9cQy5a5ITQ6c9mBrU3rszKlEOMo16DCgCLcBGAsYHQ/s1600/nantes.png"],
            ["OGC Nice", "Liga Prancis", "https://1.bp.blogspot.com/-u9a8UD9lyqM/XteJg9joUSI/AAAAAAAAK4Y/LrLh9LBfQJIbJSaTyLo0GH804P7XaKYigCLcBGAsYHQ/s1600/nice.png"],
            ["Nimes", "Liga Prancis", "https://1.bp.blogspot.com/-ItVQffa2qa0/XteJhPFQNyI/AAAAAAAAK4c/IxU6KVXvWGsLPDE-mueJiSi4HeQcA6tKQCLcBGAsYHQ/s1600/nimes.png"],
            ["Reims", "Liga Prancis", "https://1.bp.blogspot.com/-A16165DWMtA/XteJaqKV0yI/AAAAAAAAK3k/fyvsUV-oi9QNovaU8_AGTJZ61PPk5rqigCLcBGAsYHQ/s1600/Reims.png"],
            ["Rennes", "Liga Prancis", "https://1.bp.blogspot.com/-gmxij_M0ccU/XteJa_8c0LI/AAAAAAAAK3o/9Pfn_bNtMmUNnrR1JT7eXjgIgpEzsbSzwCLcBGAsYHQ/s1600/Rennes.png"],
            ["Strasbourg", "Liga Prancis", "https://1.bp.blogspot.com/-tlX7rqoZzrw/XteJhusQYmI/AAAAAAAAK4g/K0PS70je514UQdd2gCmOEgdXgrRIwd_PACLcBGAsYHQ/s1600/strasbourg.png"],
        ]);
    }
}
