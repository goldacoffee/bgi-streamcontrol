<?php

use App\WerApp\ClientDomain;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fakeData =  Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            $data = [
                'name' => $fakeData->name(),
                'url' => $fakeData->domainName,
                'contact' => $fakeData->phoneNumber,
                'billing_date' => random_int(1, 30),
                'register_date' => date('Y-m-d')



            ];
            $x = new ClientDomain(['id' => Str::uuid()]);
            $x->fill($data);
            $save =   $x->save();
            print_r($data['name'] . " added to Client \n");
        }
    }
}
