<?php

namespace App\Http\Controllers;

use App\WerApp\Football\FootballLiga;
use App\WerApp\Football\FootballMatch;
use App\WerApp\Football\FootballTeam;
use Carbon\Carbon;
use Illuminate\Http\Request;
use WerHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class ApiController extends Controller
{
    /**
     * /api/endpoint/match
     */
    public function match($id = '')
    {


        try {
            if ($id === '') {


                $data = Cache::rememberForever('match-without-id', function () {
                    $hariIni =  Carbon::now();
                    $tanggalHariIni = $hariIni->toDateString();
                    // dd($tanggalHariIni);
                    return FootballMatch::with(['home_team', 'away_team', 'liga'])

                        // ->groupBy('date')
                        ->orderBy('start_time', 'ASC')
                        //   ->where('start_time', '>=', $tanggalHariIni)
                        ->where('status', '!=', '1')
                        ->get()
                        ->groupBy('tanggal_id');
                });
            } else {

                $teamId = FootballTeam::where('slug', $id)->first();
                // $match =  FootballMatch::where('slug', $id)->with(['home_team', 'away_team', 'liga'])->first();
                $data = Cache::rememberForever('match-' . $id, function () use ($teamId) {
                    $match =  FootballMatch::where('status', '=', '0')
                        ->where('home_team_id', $teamId->id)
                        ->orWhere('away_team_id', $teamId->id)

                        ->with(['home_team', 'away_team', 'liga'])
                        ->first();

                    if ($match === null) {
                        $match =  FootballMatch::where('home_team_id', $teamId->id)
                            ->orWhere('away_team_id', $teamId)

                            ->with(['home_team', 'away_team', 'liga'])
                            ->first();
                    } else {
                        $relatedMatch = FootballMatch::where('liga_id', $match->liga_id)
                            ->where('id', '!=', $match->id)
                            ->orderBy('start_time', 'ASC')->get();
                        $match->related = $relatedMatch;
                    }

                    return $match;
                });





            }


            return WerHelper::successResponse($data);
        } catch (\Throwable $th) {
            return WerHelper::errorResponse($th);
        }
    }
    public function team($id = '')
    {

        try {
            if ($id === '') {
                $data =  FootballTeam::all();
            } else {
                $data =  FootballTeam::where('slug', $id)->with(['homeMatch', 'awayMatch',])->get();
                // $relatedMatch = FootballMatch::where('liga_id', $match->liga_id)
                //     ->where('id', '!=', $match->id)
                //     ->orderBy('start_time', 'ASC')->get();
                // $match->related = $relatedMatch;


            }


            return WerHelper::successResponse($data);
        } catch (\Throwable $th) {

            return WerHelper::errorResponse($th);
        }
    }
    public function liga($slug = '')
    {

        try {
            if ($slug === '') {
                $data = FootballLiga::all();
            } else {
                $ligas = FootballLiga::where('slug', $slug)->first();



                $ligas['teams']  = FootballTeam::where('liga_id', 'like', '%' . $ligas->id . '%')->get();
                $ligas['match']  = FootballMatch::where('liga_id', $ligas->id)->with(['home_team', 'away_team', 'liga'])->get()->groupBy('tanggal_id');;


                $data = $ligas;
            }

            return WerHelper::successResponse($data);
        } catch (\Throwable $th) {
            return WerHelper::errorResponse($th);
        }
    }
}
