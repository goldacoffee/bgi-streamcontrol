<?php

namespace App\Http\Controllers;

use App\WerApp\Football\FootballLiga;
use App\WerApp\Football\FootballMatch;
use App\WerApp\Football\FootballTeam;
use App\WerApp\Football\Match;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use WerHelper;
use Illuminate\Support\Facades\Cache;

class FootballMatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data =  FootballMatch::with('home_team', 'away_team', 'liga')->orderBy('created_at', 'desc')->get();
            return WerHelper::successResponse($data);
        } catch (\Throwable $th) {
            return WerHelper::errorResponse($th);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = [
                'liga_id' => $request->liga['id'],
                'home_team_id' => $request->home_team['id'],
                'away_team_id' => $request->away_team['id'],
                'name' => $request->home_team['name'] . ' vs ' . $request->away_team['name'],
                'featured_image' => $request->featured_image,
                'status' => ($request->status == 1 || $request->status == 0) ? $request->status : $request->status['value'],
                'stream_link' => $request->stream_link,
                'start_time' => date('Y-m-d H:i:s', strtotime($request->start_time))
            ];
            $match = new FootballMatch(['id' => Str::uuid()]);
            $match->fill($data);
            Cache::flush();
            $save =  $match->save();
            return WerHelper::successResponse($save);
        } catch (\Throwable $th) {
            return WerHelper::errorResponse($th);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WerApp\Football\Match  $match
     * @return \Illuminate\Http\Response
     */
    public function show(FootballMatch $match)
    {
        try {
            $data =  FootballMatch::with('home_team', 'away_team', 'liga')->where('id', $match->id)->first();
            Cache::flush();
            return WerHelper::successResponse($data);
        } catch (\Throwable $th) {
            return WerHelper::errorResponse($th);
        }
    }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WerApp\Football\Match  $match
     * @return \Illuminate\Http\Response
     */
    public function edit(FootballMatch $match)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WerApp\Football\Match  $match
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FootballMatch $match)
    {





        try {
            $data = [
                'liga_id' => $request->liga['id'],
                'home_team_id' => $request->home_team['id'],
                'away_team_id' => $request->away_team['id'],
                'name' => $request->home_team['name'] . ' vs ' . $request->away_team['name'],
                'featured_image' => $request->featured_image,
                'status' => ($request->status == 1 || $request->status == 0) ? $request->status : $request->status['value'],
                'stream_link' => $request->stream_link,
                'start_time' => date('Y-m-d H:i:s', strtotime($request->start_time))
            ];

            $match->fill($data);
            $save =  $match->save();

            Cache::flush();
            return WerHelper::successResponse($save);
        } catch (\Throwable $th) {
            return WerHelper::errorResponse($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WerApp\Football\Match  $match
     * @return \Illuminate\Http\Response
     */
    public function destroy(FootballMatch $match)
    {
        try {
            $delete = $match->delete();
            Cache::flush();
            return WerHelper::successResponse($delete);
        } catch (\Throwable $th) {
            return WerHelper::errorResponse($th);
        }
    }
}
