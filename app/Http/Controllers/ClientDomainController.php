<?php

namespace App\Http\Controllers;

use App\WerApp\ClientDomain;
use Illuminate\Http\Request;
use WerHelper;

class ClientDomainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data = ClientDomain::all();
            return WerHelper::successResponse($data);
        } catch (\Throwable $th) {
            return WerHelper::errorResponse($th);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        try {
            $data = [

                'name' => $request->name,
                'url' => $request->url,
                'contact' => $request->contact,
                'suspended' => $request->suspended,
                'register_date' =>  date('Y-m-d', strtotime($request->register_date)),
                'billing_date' => $request->billing_date
            ];
            $x = new ClientDomain(['id' => $request->id]);
            $x->fill($data);
            $save = $x->save();
            return WerHelper::successResponse($save);
        } catch (\Throwable $th) {
            return WerHelper::errorResponse($th);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WerApp\Model\ClientDomain  $clientDomain
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $data = ClientDomain::find($id);
            return WerHelper::successResponse($data);
        } catch (\Throwable $th) {
            return WerHelper::errorResponse($th);
            //throw $th;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WerApp\Model\ClientDomain  $clientDomain
     * @return \Illuminate\Http\Response
     */
    public function edit(ClientDomain $clientDomain)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WerApp\Model\ClientDomain  $clientDomain
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClientDomain $clientDomain)
    {







        try {
            $data = [


                'name' => $request->name,
                'url' => $request->url,
                'contact' => $request->contact,
                'suspended' => $request->suspended,
                'register_date' =>  date('Y-m-d', strtotime($request->register_date)),
                'billing_date' => $request->billing_date
            ];
            $x = ClientDomain::find($request->id);
            $save =  $x->update($data);
            return WerHelper::successResponse($save);
        } catch (\Throwable $th) {
            return WerHelper::errorResponse($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WerApp\Model\ClientDomain  $clientDomain
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        try {
            $delete = ClientDomain::find($id)->delete();
            return WerHelper::successResponse($delete);
        } catch (\Throwable $th) {
            return WerHelper::errorResponse($th);
        }
    }
}
