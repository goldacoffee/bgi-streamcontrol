<?php

namespace App\Http\Controllers;

use App\WerApp\Football\UserActivityLog;
use Illuminate\Http\Request;

class UserActivityLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WerApp\Football\UserActivityLog  $userActivityLog
     * @return \Illuminate\Http\Response
     */
    public function show(UserActivityLog $userActivityLog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WerApp\Football\UserActivityLog  $userActivityLog
     * @return \Illuminate\Http\Response
     */
    public function edit(UserActivityLog $userActivityLog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WerApp\Football\UserActivityLog  $userActivityLog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserActivityLog $userActivityLog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WerApp\Football\UserActivityLog  $userActivityLog
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserActivityLog $userActivityLog)
    {
        //
    }
}
