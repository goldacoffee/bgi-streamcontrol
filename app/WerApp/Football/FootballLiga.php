<?php

namespace App\WerApp\Football;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use WerHelper;

class FootballLiga extends Model
{
    protected $table = 'werapp_football_ligas';
    protected $fillable = [
        'id',
        'name',
        'name_alt',
        "featured_image",
        // 'slug',
        // 'slug_alt',
        'logo',
        'description',
        'bg_color',

    ];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        // 'created_at', 'updated_at',
    ];
    protected $casts = [
        'id' => 'string',
    ];

    // public function teamList()
    // {
    //     // return $this->hasMany(FootballTeam::class, 'liga_id', 'id');
    //     return $this->getAttribute('id');;
    // }
    public function matchList()
    {
        return $this->hasMany(FootballMatch::class, 'liga_id', 'id');
    }

    public function getFeaturedImageAttribute($value)
    {
        if (isset($_GET['clean'])) {
            return $value;
        } else {
            if ($value === null) {
                return "https://1.bp.blogspot.com/-HnxBX8Vz2Lo/XwOIqatSN-I/AAAAAAAALwc/e4MU0Aa4K7sH_-Cz3fVIIWcUuTJ6F-ifwCLcBGAsYHQ/s1600/epl.jpg";
            } else {
                return $value;
            }
        }
    }



    public static function getBgColorAttribute($value)
    {
        return ($value === null) ? '#046ca2' : $value;
    }

    protected static function booted()
    {
        static::saving(function ($liga) {
            $liga->slug = Str::slug($liga->name);
            $liga->slug_alt = Str::slug($liga->name_alt);
            WerHelper::saveLigaToJsonAll();
            WerHelper::saveLigaToJsonSingular(Str::slug($liga->name));
            // WerHelper::saveLigaToJsonSingular(Str::slug($liga->name_alt));
        });
    }
}
