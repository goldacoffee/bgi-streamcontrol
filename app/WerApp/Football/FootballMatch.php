<?php

namespace App\WerApp\Football;

use Carbon\Carbon;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use WerHelper;
use App\WerApp\Football\FootballTeam;

class FootballMatch extends Model
{

    protected $table = 'werapp_football_matches';
    protected $fillable = [
        'id',
        'liga_id',
        'name',
        'home_team_id',
        'away_team_id',
        'stream_link',
        'status',
        'featured_image',
        'start_time',
    ];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected $appends = ['start_time_alt', 'timestamps', 'tanggal_id', 'xml_time'];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        // 'created_at', 'updated_at',
    ];
    protected $casts = [
        'id' => 'string',
        'stream_link' => 'array',
        'start_time' => 'datetime'
    ];


    // public function getHariAttribute()
    // {
    //     return date('D m', strtotime($this->getAttribute('start_time')));
    // }


    public function getTanggalIdAttribute()
    {
        // $time = Carbon::createFromTimestamp(strtotime($this->getAttribute('start_time')));
        // $time =  $time->format('d F');
        try {

            $tanggalHari = date('Y-m-d', $this->getAttribute('timestamps'));


            // $time = $tanggalHari->format('Y-m-d');
            $time = WerHelper::tanggal_indo($tanggalHari);





            return $time;
        } catch (\Throwable $th) {
            return '';
        }
    }

    public function getXmlTimeAttribute()
    {
        return date('Y-m-d', strtotime($this->getAttribute('updated_at')));
    }
    public function getStartTimeAltAttribute()
    {
        return date('Y-m-d H:i:s', strtotime($this->getAttribute('start_time')));
    }
    public function getTimestampsAttribute()
    {
        return  strtotime($this->getAttribute('start_time'));
    }

    public function liga()
    {
        return $this->hasOne('App\WerApp\Football\FootballLiga', 'id', 'liga_id');
    }

    public function home_team()
    {
        return $this->hasOne('App\WerApp\Football\FootballTeam', 'id', 'home_team_id');
    }
    public function away_team()
    {
        return $this->hasOne('App\WerApp\Football\FootballTeam', 'id', 'away_team_id');
    }


    protected static function booted()
    {

        static::saving(function ($match) {

            $match->slug = Str::slug($match->name);
            WerHelper::saveMatchToJsonAll();

            $team = explode('-vs-', Str::slug($match->name));
            WerHelper::saveMatchToJsonSingular($team[0]);
            WerHelper::saveMatchToJsonSingular($team[1]);
        });
        static::saved(function ($match) {

            $match->slug = Str::slug($match->name);
            WerHelper::saveMatchToJsonAll();

            $team = explode('-vs-', Str::slug($match->name));
            WerHelper::saveMatchToJsonSingular($team[0]);
            WerHelper::saveMatchToJsonSingular($team[1]);
        });

        static::updated(function ($match) {

            $match->slug = Str::slug($match->name);
            WerHelper::saveMatchToJsonAll();

            $team = explode('-vs-', Str::slug($match->name));
            WerHelper::saveMatchToJsonSingular($team[0]);
            WerHelper::saveMatchToJsonSingular($team[1]);
        });

        static::deleted(function ($match) {

            $team = explode('-vs-', Str::slug($match->name));



            $team1 = public_path() . '/feed/' . $team[0] . '.json';
            $team2 = public_path() . '/feed/' . $team[0] . '.json';
            if (file_exists($team1)) {

                unlink($team1);
            }
            if (file_exists($team2)) {
                unlink($team2);
            }
            WerHelper::saveMatchToJsonAll();
        });


        //
        // . ' vs ' .  FootballLiga::where('id', $match->away_team_id)->first()[name];
    }
}
