<?php

namespace App\WerApp;

use Illuminate\Database\Eloquent\Model;

class ClientDomain extends Model
{
    protected $table = 'werapp_client_domains';
    protected $fillable = [
        'id',
        'name',
        'url',
        'contact',
        'suspended',
        'register_date',
        'billing_date'
    ];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    protected $casts = [
        'id' => 'string',
        // 'register_date' => 'date'
    ];
}
