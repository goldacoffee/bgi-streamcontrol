<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\WerApp\Football\FootballTeam;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/save', 'FootballMatchController@saveToJson');

Auth::routes();
Route::get('/logout', 'HomeController@logout')->name('logoutx');

Route::get('/', 'HomeController@welcome')->name('welcome');

Route::get('/', 'HomeController@welcome')->name('welcome');
Route::get('/football', 'HomeController@index')->name('football.dashboard');
Route::get('/basketball', 'HomeController@index')->name('basketball.dashboard');
Route::get('/{any}', 'HomeController@index')->where('any', '.*');
