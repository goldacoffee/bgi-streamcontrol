<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'endpoint'], function () {
    Route::get('match/{id?}', 'ApiController@match');
    Route::get('liga/{id?}', 'ApiController@liga');
    Route::get('team/{id?}', 'ApiController@team');
});

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'football'], function () {
    Route::resource('liga', 'FootballLigaController');
    // Route::resource('clinetdomain', 'ClientDomainController');
    Route::resource('match', 'FootballMatchController');
    Route::resource('team', 'FootballTeamController');
});
// client domain
Route::resource('domain', 'ClientDomainController@index');
